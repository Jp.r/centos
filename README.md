# Script de configuration post-installation pour les serveurs CentOS 7

(c) Niki Kovacs, 2020

Ce référentiel fournit un script de configuration post-installation "automagic" pour les serveurs exécutant CentOS 7 ainsi qu'une collection de scripts d'assistance et des modèles de fichiers de configuration pour les services communs.

## En bref

Effectuez les étapes suivantes :

  1. Installer un système CentOS 7 (minimal).

  2. Créer un utilisateur non "root" avec les privilèges d'administrateur.

  3. Installer Git: `sudo yum install git`

  4. Récupérer le script: `git clone https://gitlab.com/kikinovak/centos-7.git`

  5. Aller dans le nouveau répertoire: `cd centos-7`

  6. Exécuter le script: `sudo ./centos-setup.sh --setup`

  7. Prenez une tasse de café pendant que le script fait tout le travail.

  8. Redémarrer.


## Personnalisation d'un serveur CentOS

Transformer une installation minimale de CentOS en un serveur fonctionnel revient toujours à faire une série d'opérations qui peuvent être plus ou moins longues.
Cette série d'opérations peut varier, mais voici ce que je fais habituellement sur une nouvelle installation de CentOS:


  * Personnalisez le shell Bash : invite, alis, etc.

  * Personnalisez l'éditeur Vim.

  * Configurer des dépôts officiels et tiers.

  * Installer un ensemble complet d'outils en ligne de commande.

  * Supprimer une poignée de paquets inutiles.

  * Autoriser l'administrateur à accéder aux journaux système.

  * Désactivez l'IPv6 et reconfigurez certains services en conséquence.
  
  * Configurez un mot de passe persistant pour "sudo".

  * Etc.

Le script `centos-setup.sh` effectue toutes ces opérations.

Configurez Bash et Vim puis définissez une résolution par défaut (qui sera plus lisible) pour la console.

```
# ./centos-setup.sh --shell
```

Configurer les dépôts officiels et tiers:

```
# ./centos-setup.sh --repos
```

Installer les groupes de paquets `Core` et `Base` avec quelques outils supplémentaires:

```
# ./centos-setup.sh --extra
```

Supprimer une poignée de paquets inutiles:

```
# ./centos-setup.sh --prune
```

Autoriser l'administrateur à accéder aux journaux système:

```
# ./centos-setup.sh --logs
```

Désactivez l'IPv6 et reconfigurez certains services en conséquence:

```
# ./centos-setup.sh --ipv4
```

Configurez un mot de passe persistant pour "sudo":

```
# ./centos-setup.sh --sudo
```

Effectuer tout ce qui précède en une seule fois:

```
# ./centos-setup.sh --setup
```

Supprimez les paquets et revenez à un système de base amélioré:

```
# ./centos-setup.sh --strip
```

Afficher le message d'aide:

```
# ./centos-setup.sh --help
```

Si vous voulez savoir ce qui se passe exactement sous le capot, ouvrez un second terminal et affichez les journaux:

```
$ tail -f /tmp/centos-setup.log
```

